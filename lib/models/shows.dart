class Show {
  final String name; 
  final Images image;
  final String premiered;
  final String summary;

  Show({this.name, this.image, this.premiered,this.summary}); 

  factory Show.fromJson(Map<String, dynamic> json) {
    return Show(
      name: json["name"],
      premiered: json["premiered"],
      summary: json["summary"],
      image: json['image'] != null ? new Images.fromJson(json['image']) : null);
  }

}

class Images {
  String medium;

  Images({this.medium});

  Images.fromJson(Map<String, dynamic> json) {
    medium = json['medium'];
  }
}