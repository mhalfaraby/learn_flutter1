import 'dart:developer';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tvmaze_sagara_intern/widgets/showsWidget.dart';
import 'package:tvmaze_sagara_intern/service.dart';

void main() {
  runApp(ChangeNotifierProvider(create: (_)=>Service(),
    child: MyApp()));
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState(){
    super.initState();
    Provider.of<Service>(context,listen: false).populateAllShow();

  }

  @override
  Widget build(BuildContext context) {
    final sw =  Provider.of<Service>(context);

      return 
              MaterialApp(
          home: Scaffold(
            appBar: AppBar(
              title: Text("TV MAZE"),
            ),
            body: ShowWidget(shows: sw.shows),
          ),
      );
       
  }
}
