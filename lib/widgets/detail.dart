import 'package:flutter/material.dart';
import '../models/shows.dart';

class Detail extends StatelessWidget {
  final Show show;

  Detail({Key key, this.show}) : super(key: key); 
String removeAllHtmlTags(String htmlText) {
    RegExp exp = RegExp(
      r"<[^>]*>",
      multiLine: true,
      caseSensitive: true
    );

    return htmlText.replaceAll(exp, '');
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Detail"),
      ),
      body: ListView(children: <Widget>[
          Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
             Expanded(
                            child: Padding(
                              padding: const EdgeInsets.all(5.0),
                              child: Container(alignment: Alignment.topLeft,    margin: EdgeInsets.only(top: 10,left:10 ,bottom: 10),
                width: 250.0,
                height: 250.0,
                child:  Image.network(show.image.medium),
            ),
                            ),
             ),
             Expanded(
              child:  Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                   Row(
                    children: <Widget>[
                       Expanded(
                        child:  Container(
                          margin: const EdgeInsets.only(
                              top: 12.0, bottom: 10.0),
                          child:  Text(show.name,
                            style:  TextStyle(
                              fontSize: 18.0,
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                   Container(
                    margin: const EdgeInsets.only(right: 10.0),
                    child:   Text(removeAllHtmlTags(show.summary),
                      style:  TextStyle(
                        fontSize: 16.0,
                      ),
                      maxLines: 10,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                   Container(
                    margin: const EdgeInsets.only(top: 10.0),
                    child:  Row(
                      children: <Widget>[
                         Text("RELEASED",
                          style:  TextStyle(
                            color: Colors.grey[500],
                            fontSize: 11.0,
                          ),
                        ),
                         Container(
                            margin: const EdgeInsets.only(left: 5.0),
                            child:  Text(show.premiered)
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        )
      ]),
    );
  }
}
