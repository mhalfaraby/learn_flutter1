import 'package:flutter/material.dart';
import 'package:tvmaze_sagara_intern/models/shows.dart';
import 'package:tvmaze_sagara_intern/widgets/detail.dart';

class ShowWidget extends StatelessWidget {
  final List<Show> shows;

  ShowWidget({this.shows});

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
        ),
        itemCount: shows.length,
        itemBuilder: (context, index) {
          final show = shows[index];

          return InkWell(
              child: Container(
                padding:
                    EdgeInsets.only(left: 10, top: 10, right: 10, bottom: 20),
                child: Column(
                  children: <Widget>[
                    Expanded(
                      flex: 5,
                      child: Image.network(
                        show.image.medium,
                        // height: 150.0,
                        fit: BoxFit.cover,
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: Text(
                        show.name,
                        textAlign: TextAlign.center,
                      ),
                    )
                  ],
                ),
              ),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => Detail(show: shows[index]),
                    ));
              });
        });
  }
}
