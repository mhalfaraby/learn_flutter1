import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'models/shows.dart';

class Service extends ChangeNotifier {
List<Show> shows = [];
void populateAllShow() async{
      this.shows  = await _fetchAllTvShows();
      notifyListeners();
  }
Future <List<Show>> _fetchAllTvShows() async {

   try {
      Response response = await Dio().get("https://api.tvmaze.com/shows");
      return (response.data as List)
          .map((x) => Show.fromJson(x))
          .toList();
    } catch (error) {
      throw Exception("Exception occured: $error");
    }
  }
}